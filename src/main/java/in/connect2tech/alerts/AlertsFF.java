package in.connect2tech.alerts;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class AlertsFF {

	public static void main(String[] args) {
		System.setProperty("webdriver.firefox.marionette", "D:/nchaurasia/Automation-Architect/connect2tech.in-SeleniumAutomation/drivers/geckodriver.exe");
		WebDriver driver = new FirefoxDriver();
		// driver.get("http://cookbook.seleniumacademy.com/Alerts.html");
		driver.get(
				"file:///D:/nchaurasia/Automation-Architect/connect2tech.in-SeleniumAutomation/src/main/java/in/connect2tech/alerts/Alert.html");
		driver.manage().window().maximize();
		
		driver.findElement(By.id("prompt")).click();
		
		Alert alert = driver.switchTo().alert();
		alert.sendKeys("hello");
	}

}
